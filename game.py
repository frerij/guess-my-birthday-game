#setting up random number generator 
from random import randint

#asking for user's name input
name = input("Hi! What is your name?")
print("Great! Nice to meet you,", name)

#add user input for number of guesses the computer gets?

#setting up to loop 5 times 
for x in range(5):
    guess_number = x + 1
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    #printing guess
    print("Guess", guess_number, ";", name, "were you born in", month_number, "/", year_number, "?")
    response = input("yes or no?\n")
    
    #checks guess based on user input and then re-guesses or ends loop
    if (response == "yes"):
        print("I knew it!")
        break
    elif (response == "no" and guess_number == 5):
        print("I have other things to do. Good bye.")
    elif (response == "no"):
        print("Drat! Lemme try again!")
    else:
        print("Not an acceptable answer.")
        input("Please type yes or no\n")


        #stretch goals:
        #make replies "yes, later, earlier" and adjust computer guesses accordingly
        #add the day to the guess as well
        #have the computer guess the month by name instead of number

